package org.pbank.san.interactor.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="t_customer")
public class CustomerEntity extends BaseEntity<Integer> implements Serializable {
	
	@Column(name="nombre")
	private String name;
	
	@Column(name="dni")
	private String identificator;
	
	@Column(name="telefono")
	private Integer phone;
	
	// bi-directional one-to-many association to Credit Card
	@OneToMany(fetch=FetchType.LAZY, mappedBy = "customer")
	private List<CreditCardEntity> creditCards;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIdentificator() {
		return identificator;
	}

	public void setIdentificator(String identificator) {
		this.identificator = identificator;
	}

	public Integer getPhone() {
		return phone;
	}

	public void setPhone(Integer phone) {
		this.phone = phone;
	}

	public List<CreditCardEntity> getCreditCards() {
		return creditCards;
	}

	public void setCreditCards(List<CreditCardEntity> creditCards) {
		this.creditCards = creditCards;
	}

}
