package org.pbank.san.interactor.entities;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

@MappedSuperclass
public class BaseEntity<K> {
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private K id;

	@Column(name = "ts_creado")
	private int tsCreate;
	@Column(name = "ts_editado")
	private Integer tsUpdate;
	@Column(name = "us_creado")
	private int usCreate;
	@Column(name = "us_editado")
	private Integer usUpdate;
	
    @PrePersist
    public void prePersist(){    	
    	this.setTsCreate( (int) (System.currentTimeMillis()/10000));
    }
	
    @PreUpdate
    public void preUpdate(){    	
    	this.setTsUpdate( (int) (System.currentTimeMillis()/10000));
    }
	public K getId() {
		return id;
	}

	public void setId(K id) {
		this.id = id;
	}

	public int getTsCreate() {
		return tsCreate;
	}

	public void setTsCreate(int tsCreate) {
		this.tsCreate = tsCreate;
	}

	public Integer getTsUpdate() {
		return tsUpdate;
	}

	public void setTsUpdate(Integer tsUpdate) {
		this.tsUpdate = tsUpdate;
	}

	public int getUsCreate() {
		return usCreate;
	}

	public void setUsCreate(int usCreate) {
		this.usCreate = usCreate;
	}

	public Integer getUsUpdate() {
		return usUpdate;
	}

	public void setUsUpdate(Integer usUpdate) {
		this.usUpdate = usUpdate;
	}
}
