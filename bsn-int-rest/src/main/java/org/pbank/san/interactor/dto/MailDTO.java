package org.pbank.san.interactor.dto;

public class MailDTO {
	
	private String identificator;
	
	private String mail;

	public String getIdentificator() {
		return identificator;
	}

	public void setIdentificator(String identificator) {
		this.identificator = identificator;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

}
