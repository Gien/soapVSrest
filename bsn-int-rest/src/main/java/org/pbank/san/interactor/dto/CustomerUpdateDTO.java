package org.pbank.san.interactor.dto;

public class CustomerUpdateDTO extends CustomerDTO {

	private int id;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	
}
