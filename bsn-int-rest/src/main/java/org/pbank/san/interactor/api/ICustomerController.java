package org.pbank.san.interactor.api;

import javax.validation.Valid;

import org.jboss.logging.Param;
import org.pbank.san.interactor.dto.CustomerDTO;
import org.pbank.san.interactor.dto.CustomerQueryDTO;
import org.pbank.san.interactor.dto.CustomerUpdateDTO;
import org.pbank.san.interactor.dto.CustomersDTO;
import org.pbank.san.interactor.dto.MailDTO;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

public interface ICustomerController {
    
    @RequestMapping(value = "/customer",
        produces = { "application/json" }, 
        consumes = { "application/json" },
        method = RequestMethod.POST)
    default ResponseEntity<Void> addCustomer(@Valid @RequestBody CustomerDTO body) {
        // do some magic!
        return new ResponseEntity<Void>(HttpStatus.OK);
    }
    
    @RequestMapping(value = "/customer",
        produces = { "application/json" }, 
        consumes = { "application/json" },
        method = RequestMethod.PUT)
    default ResponseEntity<Void> putCustomer(@Valid @RequestBody CustomerUpdateDTO body) {
        // do some magic!
        return new ResponseEntity<Void>(HttpStatus.OK);
    }
    
    @RequestMapping(value = "/customer/get",
        produces = { "application/json" }, 
        consumes = { "application/json" },
        method = RequestMethod.POST)
    default ResponseEntity<CustomersDTO> getCustomer(@Valid @RequestBody CustomerQueryDTO body) {
        // do some magic!
        return new ResponseEntity<CustomersDTO>(HttpStatus.OK);
    }

    @RequestMapping(value = "/customer/del",
            produces = { "application/json" }, 
            consumes = { "application/json" },
            method = RequestMethod.POST)
    ResponseEntity<Void> delCustomer(@Valid @RequestBody String identificator) throws Exception;

    @RequestMapping(value = "/customer/mail",
            produces = { "application/json" }, 
            consumes = { "application/json" },
            method = RequestMethod.POST)
    ResponseEntity<Void> mailCustomer(@Valid @RequestBody MailDTO mail) throws Exception;


    @RequestMapping(value = "/customer/pdedido",
            method = RequestMethod.GET)
    ResponseEntity<String> httpResponse() throws Exception;

}
